package com.lyj.pcmanagementapi.service;

import com.lyj.pcmanagementapi.entity.PcManagement;
import com.lyj.pcmanagementapi.enums.UpdateType;
import com.lyj.pcmanagementapi.model.PcManagementRequest;
import com.lyj.pcmanagementapi.model.PcManagementResponse;
import com.lyj.pcmanagementapi.model.UpdateTypeItem;
import com.lyj.pcmanagementapi.repository.PcManagementRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor


public class PcManagementService {
    private final PcManagementRepository pcManagementRepository;

    public void setPcManagement(PcManagementRequest request) {
        PcManagement addData = new PcManagement();
        addData.setSeat(request.getSeat());
        addData.setDateUpdate(LocalDate.now());
        addData.setUpdateType(request.getUpdateType());
        addData.setDateCheck(LocalDate.now());
        addData.setInspectionType(request.getInspectionType());
        addData.setRunStatus(request.getRunStatus());
        addData.setIsRepair(request.getIsRepair());
        addData.setEtcText(request.getEtcText());

        pcManagementRepository.save(addData);
    }

    public List<UpdateTypeItem> getUpdateTypes(){
        List<PcManagement> originList = pcManagementRepository.findAll();

        List<UpdateTypeItem> result = new LinkedList<>();

        for(PcManagement pcManagement : originList){
            UpdateTypeItem addItem = new UpdateTypeItem();
            addItem.setId(pcManagement.getId());
            addItem.setSeat(pcManagement.getSeat());
            addItem.setDateUpdate(pcManagement.getDateUpdate());
            addItem.setUpdateType(pcManagement.getUpdateType().getContents());
            addItem.setRunStatus(pcManagement.getRunStatus().getState());

            result.add(addItem);
        }

        return result;
    }

    public PcManagementResponse getPcManagement(long id) {
        PcManagement orginData = pcManagementRepository.findById(id).orElseThrow();
        //데이터를 찾으러 갔는데 있을수도 없을수도 있는 상황이다.
        //없을 경우엔 중단하라는 뜻 : orElseThrow()

        PcManagementResponse response = new PcManagementResponse();
        response.setId(orginData.getId());
        response.setSeat(orginData.getSeat());
        response.setDateUpdate(orginData.getDateUpdate());
        response.setUpdateTypeContent(orginData.getUpdateType().getContents());
        response.setDateCheck(orginData.getDateCheck());
        response.setRunStatusState(orginData.getRunStatus().getState());
        response.setInspectionTypeContents(orginData.getInspectionType().getContents());
        response.setIsRepairState(orginData.getIsRepair()? "예" : "아니오");
        response.setEtcText(orginData.getEtcText());

        return response;
    }
}
