package com.lyj.pcmanagementapi.cotroller;

import com.lyj.pcmanagementapi.model.PcManagementRequest;
import com.lyj.pcmanagementapi.model.PcManagementResponse;
import com.lyj.pcmanagementapi.model.UpdateTypeItem;
import com.lyj.pcmanagementapi.service.PcManagementService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/pc")

public class PcManagementController {
    private final PcManagementService pcManagementService;

    @PostMapping("/new")
    public String setPcManagement(@RequestBody PcManagementRequest request) {
        pcManagementService.setPcManagement(request);

        return "OK";
    }

    @GetMapping("/update")
    public List<UpdateTypeItem> getPcManagement(){return pcManagementService.getUpdateTypes();
    }

    @GetMapping("/detail/{id}") //{id} id라는 변수로 인지한다.
    public PcManagementResponse getPcManagement(@PathVariable long id) {
        return pcManagementService.getPcManagement(id);
    }

}
