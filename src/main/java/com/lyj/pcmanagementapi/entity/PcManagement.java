package com.lyj.pcmanagementapi.entity;

import com.lyj.pcmanagementapi.enums.InspectionType;
import com.lyj.pcmanagementapi.enums.RunStatus;
import com.lyj.pcmanagementapi.enums.UpdateType;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Entity
@Getter
@Setter

public class PcManagement {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false, length = 10)
    private Short seat;


    private LocalDate dateUpdate;

    @Column(nullable = false, length = 20)
    @Enumerated(value = EnumType.STRING)
    private UpdateType updateType;

    private LocalDate dateCheck;

    @Column(nullable = false, length = 20)
    @Enumerated(value = EnumType.STRING)
    private InspectionType inspectionType;

    @Column(nullable = false, length = 20)
    @Enumerated(value = EnumType.STRING)
    private RunStatus runStatus;

    @Column(nullable = false)
    private Boolean isRepair;

    @Column(columnDefinition = "TEXT")
    private String etcText;


}
