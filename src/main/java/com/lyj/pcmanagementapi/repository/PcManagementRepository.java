package com.lyj.pcmanagementapi.repository;


import com.lyj.pcmanagementapi.entity.PcManagement;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PcManagementRepository extends JpaRepository<PcManagement,Long> {
}
