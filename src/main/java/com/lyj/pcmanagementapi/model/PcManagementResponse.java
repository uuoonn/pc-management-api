package com.lyj.pcmanagementapi.model;


import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter

public class PcManagementResponse {
    private Long id;
    private Short seat;
    private LocalDate dateUpdate;
    private String updateTypeContent;
    private LocalDate dateCheck;
    private String inspectionTypeContents;
    private String runStatusState;
    private String isRepairState;
    private String etcText;
}
