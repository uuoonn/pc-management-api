package com.lyj.pcmanagementapi.model;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class UpdateTypeItem {
    private Long id;
    private Short seat;
    private LocalDate dateUpdate;
    private String updateType;
    private String runStatus;
}
