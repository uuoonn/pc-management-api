package com.lyj.pcmanagementapi.model;

import com.lyj.pcmanagementapi.enums.InspectionType;
import com.lyj.pcmanagementapi.enums.RunStatus;
import com.lyj.pcmanagementapi.enums.UpdateType;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter

public class PcManagementRequest {

    private Short seat;

    private LocalDate dateUpdate;

    @Enumerated(value = EnumType.STRING)
    private UpdateType updateType;

    private LocalDate dateCheck;

    @Enumerated(value = EnumType.STRING)
    private InspectionType inspectionType;

    @Enumerated(value = EnumType.STRING)
    private RunStatus runStatus;

    private Boolean isRepair;

    private String etcText;


}
