package com.lyj.pcmanagementapi.enums;

import jakarta.persistence.Entity;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor

public enum InspectionType {
    CHECK_MONITOR("모니터 점검"),
    CHECK_KEYBOARD("키보드 점검"),
    CHECK_MOUSE("마우스 점검"),
    CHECK_SPEAKER("스피커 점검"),
    CHECK_HEADSET("헤드셋 점검"),
    CHECK_DESKTOP("본체 점검");

    private final String contents;

}
